# CentiBudget Roadmap
**Note that this document is a work in progress, and will be changed as new features are added. A revision number and revision date are held immediately below this notice**  
*Revision: 1 (15-14-23)*  
This is a roadmap for the CentiBudget program. It lists out the current features and defines future goals for the project. New revisions of this document are planned for monthly release, and new releases will ideally occur on a monthly basis as well. Note that because this is a passion project being done while the lead developer is still in school, release schedules may not line up neatly with this plan.

## Legend
|Abbreviation|Longform|Description|
|:-|:-|:-|
|CF|Core Feature|Codebase for these features are resistant to change|
|UFI|Up for Improvement|Current feature that is being discussed for improvement|
|PI|Proposed Improvement|Proposed improvement to a current feature, lists feature number (cf-2 for improvement for current feature 2, fp-3 for future plans 3)|
|PF|Planned Feature|Feature will be coming in a future release, sometime within the next few releases|
|PFL|Planned Feature in Limbo|feature will be coming in a future release, indefinitely on hold|
|RF|Release Feature|Blocking feature for the next release|
|IRF|Intended Release Feature|Feature planned for next release|
|UF|Unlikely Feature|Feature may never be added because of technical complexity or scope issues|

## Current features
1. *cf*: Cross-platform (Doesn't use any libraries that wouldn't be expected in a standard python installation on Mac, Linux, and Windows)
2. *ufi*: Singular source of income (UTC)
3. *ufi*: Budget calculation for specified number of weeks
4. *ufi*: 15% assumption for taxes
5. *cf*: Hourly wage budgeting mode

## Future plans
1. *uf*: GUI for doing everything (Would require refactoring the current linear workflow, will probably be broken off into a separate repository at some point)
2. *uf*: automatic testing and releasing (This is a passion project that doesn't have time to get into the nitty gritty of CI/CD at the moment, will be considered as project gains traction and notoriety)
3. *pi(cf-2)*: calculation for multiple income streams at once
4. *pf*: Salary budgeting mode
5. *pf*: Social security budgeting mode
6. *uf*: analytics for "Other" mode usage (Would require running and securing a server to hold these analytics, users can currently open up issues for their wage modes in the gitlab repo instead)
7. *pi(cf-3)*: Budget calculation for months, years
8. *uf*: android port
9. *uf*: ios port
10. *pf,cf*: Calculating budget for actual income
11. *uf*: Budget recommendations based on current recurring expenses (A budget should be driven by the user, not their spending habits)
