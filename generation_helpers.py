#!/usr/bin/python3
"""
Contains helpers for the budget generation function
"""
import datetime

# TODO: figure out how to provide hint about type this should return
def calculate_end_date(start_date, budget_duration_in_weeks):
    """
    Calculates end date for a budget when given the duration and start date

    Returns:
        Date: end date for budget
    """
    budget_duration_delta = datetime.timedelta(weeks=budget_duration_in_weeks)
    return start_date+budget_duration_delta
