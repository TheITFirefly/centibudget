# CentiBudget

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/TheITFirefly/centibudget.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/TheITFirefly/centibudget/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

## Description
CentiBudget is a percentage-based budgeting solution written in python.

## Installation
This project is written with the python standard libraries in mind, and is tested against python 3.10. If you can get a python environment set up, this project should run for you. See [python's website](https://www.python.org/) if you don't already have a python environment set up on your system

## Usage
centi_budget.py is the file that contains the main() function for the program. Running the program should be as simple as `python3 centi_budget.py` once you are in the repository's directory

## Getting Help
The best way to get help for now is to just open an issue about it right here in the centibudget project. Don't forget to check the [roadmap](roadmap.md) before making a feature request

## Roadmap
See the [project roadmap](roadmap.md) for more details on the planned features for the project

## Contributing
Contributions are absolutely welcome, just remember to stick to the standard libraries :smile:  
Methodology for branching/merging are currently being worked out

## License
Just a reminder, this project is FOSS (Free and Open Source Software) under the GPLv3. See the [LICENSE](LICENSE) for more details
