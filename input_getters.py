#!/usr/bin/python3
"""
This module contains functions for getting input from users in various formats
"""
import input_validators as iv

def get_money_input() -> float:
    """
    Prompts user to input a positive money ammount, validates input

    Returns:
        float: validated input as a float
    """
    while True:
        input_valid = False
        try:
            user_input = input("Enter money amount with decimal point and 2 digits: $")
            input_valid = iv.validate_money(user_input)
            if input_valid is False:
                raise ValueError("Invalid input. Please try again.")
            break
        except ValueError as error_output:
            print(str(error_output))
    return float(user_input)

def get_wage_mode_input() -> str:
    """
    Prompts user to input one of the wage mode options, validates input

    Returns:
        str: selected wage mode
    """
    while True:
        input_valid = False
        try:
            user_input = str(input("Enter Salary, Hourly, or Other: ").lower())
            input_valid = iv.validate_wage_mode(user_input)
            if input_valid is False:
                raise ValueError("Input was not a valid option. Try again.")
            break
        except ValueError as error_output:
            print(str(error_output))
    return user_input

def get_percentage_input() -> int:
    """
    Prompts user to input an integer from 0 to 100, validates it

    Returns:
        int: validated input as an integer
    """
    while True:
        try:
            category_percentage = int(input("Input category percentage number: "))
            if category_percentage <= 0 or category_percentage > 100:
                raise ValueError("Percentage must be between 0 and 100")
            break
        except ValueError as error_output:
            print(str(error_output))
    return category_percentage

def get_int_input() -> int:
    """
    Prompts user to input a postitive integer, validates input
    
    Returns:
        int: validated input as an integer
    """
    while True:
        input_valid = False
        try:
            user_input = input("Enter a positive integer: ")
            input_valid = iv.validate_int(user_input)
            if input_valid is False:
                raise ValueError("Input was not a positive integer. Try again")
            break
        except ValueError as error_output:
            print(str(error_output))
    return int(user_input)
