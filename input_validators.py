#!/usr/bin/python3
"""
This module contains functions for validating user input
"""
import re

def validate_money(user_input) -> bool:
    """
    Ensures that input is a valid float

    Returns:
        bool: whether or not input input is a positive float with 2 digits
    """
    money_pattern = re.compile(r'^\d+\.\d{2}$')
    if bool(money_pattern.search(user_input)) is True:
        user_input = float(user_input)
        if user_input == 0.00:
            return False
        return True
    return False

def validate_int(user_input) -> bool:
    """
    Ensures that input is a valid int

    Returns:
        bool: whether or not input is a positive integer
    """
    try:
        user_input = int(user_input)
        if user_input > 0:
            return True
        else:
            return False
    except ValueError:
        return False

def validate_wage_mode(user_input) -> bool:
    """
    Ensures that input is a valid wage mode

    Returns:
        bool: whether or not input is in the list of supported modes
    """
    valid_wage_modes = ["salary","hourly","other"]
    return bool(user_input in valid_wage_modes)

#def validate_percentage(user_input) -> bool:
#    """
#    Ensures that input is a valid percentage number
#
#    Returns:
#        bool: whether or not input is a number from 1 to 100
#    """
#    return False
