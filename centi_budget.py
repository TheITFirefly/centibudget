#!/usr/bin/env python3
"""
This module contains the main function of CentiBudget, and all helper functions written for main
"""
import os
import sys
import datetime
import json
import input_getters as ig
import generation_helpers as gh

# Constants
REPO_URL = "https://gitlab.com/TheITFirefly/centibudget"

def get_platform_path() -> str:
    """
    Sets file path based on OS

    Returns:
        string: file path for persistent storage in the user's home directory
    """
    platform = sys.platform
    storage_directory = os.path.expanduser("~")
    if platform == 'linux':
        storage_directory = os.path.join(storage_directory, ".local", "share", "centibudget")
    elif platform == 'darwin':
        storage_directory = os.path.join(storage_directory, "Library", "centibudget")
    elif platform == 'win32':
        storage_directory = os.path.join(storage_directory, "AppData", "centibudget")
    else:
        raise OSError(f"Unsupported platform: {platform}")
    return storage_directory
DIRECTORY_PATH = get_platform_path()

# Ensure that storage directory exists before main runs
os.makedirs(DIRECTORY_PATH, exist_ok=True)

def generate_budget() -> dict[str,int]:
    """
    Budget generation mode

    Returns:
        dictionary(category_name,category_percentage): 
    """
    current_date = datetime.date.today()
    print(f"Budget calculation will start from today ({current_date}).")
    print("How many weeks do you want to calculate your budget for?")
    budget_duration_in_weeks = ig.get_int_input()
    end_date = gh.calculate_end_date(datetime.date.today(), budget_duration_in_weeks)
    print(f"Budget calculation will be performed for {budget_duration_in_weeks} weeks")
    print(f"Budget ends on {end_date}")

    print("Are you calculating your budget on a salary, an hourly wage, or something else?")
    wage_mode = ig.get_wage_mode_input()
    if wage_mode == "other":
        # pylint: disable-next=line-too-long
        print(f"Unfortunately, this program does not support your current wage system for budgeting. If you would like to see support for this system, please open an issue at {REPO_URL} or consider sponsoring its development")
        sys.exit()

    if wage_mode == "hourly":
        print("What is your hourly wage?")
        hourly_wage = ig.get_money_input()

        print("Moving on to budget category creation.")
        print("Adding default category for \"Taxes\" at 15%")

        num_categories = 0
        budget_categories = {"Taxes": 15}
        category_percent_total = budget_categories["Taxes"]
        done_adding_categories = False
        while not done_adding_categories:
            print(f"You currently have {num_categories} categories.")
            print(f"Your categories add up to {category_percent_total} percent.")
            if category_percent_total == 100:
                break
            category_name = input("Enter budget category name (or 'done' if finished adding): ")
            if category_name.lower() == "done":
                done_adding_categories = True
            else:
                category_percentage = ig.get_percentage_input()
                if (category_percent_total + category_percentage) > 100:
                    print("Percentage input takes total above 100%. Please try a lower percentage")
                else:
                    budget_categories[category_name] = category_percentage
                    category_percent_total += category_percentage
                    num_categories += 1

        print("Calculating wage and budget categories. Assuming 40 hour work week")
        total_wage = hourly_wage * 40 * budget_duration_in_weeks
        print(f"Total wages for {budget_duration_in_weeks} week/s: {total_wage}")
        total_budgeted = 0.0
        for category, percentage in budget_categories.items():
            budget_wage = total_wage*(percentage/100)
            print(f"Amount allocated for {category}: ${budget_wage:.2f}")
            total_budgeted += budget_wage
        unbudgeted = total_wage - total_budgeted
        print(f"You currently have ${unbudgeted:.2f} that hasn't been budgeted")

        budget_json = json.dumps(budget_categories)
        budget_file_name = f"budget-{current_date}.json"
        budget_file_path = os.path.join(DIRECTORY_PATH, budget_file_name)
        with open(budget_file_path, "w", encoding="utf-8") as budget_file:
            budget_file.write(budget_json)
            budget_file.write('\n')
    elif wage_mode == "salary":
        budget_categories = {}
        print("Salary mode is a work in progress")
        sys.exit(0)
    else:
        print("Error: Program in an unknown state. Terminating to prevent potential system damage")
        sys.exit(1)

def main():
    """
    Main function for CentiBudget, a percentage-based budgetting program

    Returns:
        int: 0 for success, error codes if any
    """
    #DEBUG:print(f"Platform is: {sys.platform}, file path is:{DIRECTORY_PATH}")
    print("Welcome to centiBudget, a percentage-based budgeting application.)")

    generate_budget()

if __name__ == "__main__":
    main()
